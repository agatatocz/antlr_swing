tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}

prog           : (e+=expr | d+=declaration)* -> program(name={$e}, declarations={$d});
declaration    : ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> declare(n={$ID.text})
        ;
catch [RuntimeException ex] {errorID(ex,$i1);}

expr: ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
    | ^(MINUS e1=expr e2=expr) -> diff(p1={$e1.st},p2={$e2.st})
    | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
    | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
    | ^(PODST i1=ID   e2=expr) {globals.getSymbol($ID.text);} -> assign(n={$ID.text},p2={$e2.st})
    | ^(EQUAL  e1=expr e2=expr)    -> compare(p1={$e1.st},p2={$e2.st})
    | ^(NOTEQUAL  e1=expr e2=expr) -> compare(p1={$e1.st},p2={$e2.st})
    | ^(IF e1=expr e2=expr e3=expr?) {numer++;} -> condition(i={$e1.st}, id={numer.toString()}, t={$e2.st}, e={$e3.st})
    | INT                                       -> int(i={$INT.text})
    | ID  {globals.getSymbol($ID.text);}        -> id(n={$ID.text})
    ;
catch [RuntimeException ex] {errorID(ex,$i1);}

